#!/usr/bin/env bash



function deploy {
    cd website
    echo 'removing old dist'
    rm -rf ./dist
    echo 'building new dist'
    ng build --prod
    echo 'removing from aws'
    aws s3 rm s3://cloud.vr-cade.nl --recursive --profile yvan
    echo 'moving to aws'
    aws s3 cp ./dist/website s3://cloud.vr-cade.nl --recursive --acl public-read --profile yvan
}

function noDeploy {
    echo 'Coward!'
}

while true; do
    read -p "Do you want to deploy?" yn
    case $yn in
        [Yy]* ) deploy; break;;
        [Nn]* ) noDeploy; exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
