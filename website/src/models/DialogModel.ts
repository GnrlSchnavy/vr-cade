export class DialogModel {
    id: number;
    link?: string;
    title?: string;
    subTitle?: string;
    description?: string;
    bullets?: string[];
    photo?: string;
}
