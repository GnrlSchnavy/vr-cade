import {Component, NgModule, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {MyDialogComponent} from '../my-dialog/my-dialog.component';
// @ts-ignore
import jsonData from '../../../assets/i18n/nl.json';


@NgModule({
})

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  private reservationLink: string;

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  openDialog(id: number) {
    this.dialog.open(MyDialogComponent, {
      width: '50%',
      data: {
        id: id,
        link: this.getReservationLink(id),
        title: jsonData['SERVICES.POPUP.TITLE' + id.toString()],
        subTitle: jsonData['SERVICES.POPUP.SUBTITLE' + id.toString()],
        description: jsonData['SERVICES.POPUP.DESCRIPTION' + id.toString()],
        photo: jsonData['SERVICES.POPUP.PHOTO' + id.toString()],
        bullets: jsonData['SERVICES.POPUP.BULLETS' + id.toString()]
      },
    });
  }

private getReservationLink(id: number) {
  switch (id) {
    case 1: {
      return this.reservationLink = jsonData['VREXPERIENCE.RESERVATIONLINK'];
    }
    case 2: {
      return this.reservationLink = jsonData['VRLASERGAMEN.RESERVATIONLINK'];
    }
    case 3: {
      return this.reservationLink = jsonData['VRBEDRIJFSUITJE.RESERVATIONLINK'];
    }
    case 4: {
      return this.reservationLink = jsonData['VRRACEN.RESERVATIONLINK'];
    }
    case 5: {
      return this.reservationLink = jsonData['VRESCAPEROOM.RESERVATIONLINK'];
    }
    case 6: {
      return this.reservationLink = jsonData['VRKINDERFEEST.RESERVATIONLINK'];
    }
  }
}
}
