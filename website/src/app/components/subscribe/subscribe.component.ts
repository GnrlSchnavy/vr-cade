import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css']
})
export class SubscribeComponent implements OnInit {

  subscribeForm:FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.subscribeForm = this.fb.group({
      'voornaam' : [null,[Validators.required,Validators.email]],
      'achternaam' : [null,[Validators.required,Validators.email]],
      'email' : [null,[Validators.required,Validators.email]],
      'password' : [null,[Validators.required]],
      'bevestigwachtwoord' : [null,[Validators.required]]
    });
  }
  subscribe(formData:NgForm){
    console.log(formData);
  }

}
