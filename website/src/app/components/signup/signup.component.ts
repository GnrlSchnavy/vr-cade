import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {


  signupForm:FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.signupForm = this.fb.group({
      'voornaam' : [null,[Validators.required,Validators.email]],
      'achternaam' : [null,[Validators.required,Validators.email]],
      'email' : [null,[Validators.required,Validators.email]],
      'password' : [null,[Validators.required]],
      'bevestigwachtwoord' : [null,[Validators.required]]
    });
  }
  signup(formData:NgForm){
    console.log(formData);
  }

}
