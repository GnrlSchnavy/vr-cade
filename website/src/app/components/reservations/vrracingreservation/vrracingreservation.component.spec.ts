import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VrracingreservationComponent } from './vrracingreservation.component';

describe('VrracingreservationComponent', () => {
  let component: VrracingreservationComponent;
  let fixture: ComponentFixture<VrracingreservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VrracingreservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VrracingreservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
