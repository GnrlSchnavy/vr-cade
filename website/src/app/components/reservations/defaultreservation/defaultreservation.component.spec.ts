import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultreservationComponent } from './defaultreservation.component';

describe('DefaultreservationComponent', () => {
  let component: DefaultreservationComponent;
  let fixture: ComponentFixture<DefaultreservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultreservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultreservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
