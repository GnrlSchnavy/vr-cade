import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VrlasergamenreservationComponent } from './vrlasergamenreservation.component';

describe('VrlasergamenreservationComponent', () => {
  let component: VrlasergamenreservationComponent;
  let fixture: ComponentFixture<VrlasergamenreservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VrlasergamenreservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VrlasergamenreservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
