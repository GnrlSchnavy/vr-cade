import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VrkinderfeestreservationComponent } from './vrkinderfeestreservation.component';

describe('VrkinderfeestreservationComponent', () => {
  let component: VrkinderfeestreservationComponent;
  let fixture: ComponentFixture<VrkinderfeestreservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VrkinderfeestreservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VrkinderfeestreservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
