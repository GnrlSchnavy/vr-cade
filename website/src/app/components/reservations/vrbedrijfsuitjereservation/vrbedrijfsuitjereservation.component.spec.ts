import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VrbedrijfsuitjereservationComponent } from './vrbedrijfsuitjereservation.component';

describe('VrbedrijfsuitjereservationComponent', () => {
  let component: VrbedrijfsuitjereservationComponent;
  let fixture: ComponentFixture<VrbedrijfsuitjereservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VrbedrijfsuitjereservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VrbedrijfsuitjereservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
