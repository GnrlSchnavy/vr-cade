import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VrescaperoomreservationComponent } from './vrescaperoomreservation.component';

describe('VrescaperoomreservationComponent', () => {
  let component: VrescaperoomreservationComponent;
  let fixture: ComponentFixture<VrescaperoomreservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VrescaperoomreservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VrescaperoomreservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
