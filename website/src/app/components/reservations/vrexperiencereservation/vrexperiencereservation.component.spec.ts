import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VrexperiencereservationComponent } from './vrexperiencereservation.component';

describe('VrexperiencereservationComponent', () => {
  let component: VrexperiencereservationComponent;
  let fixture: ComponentFixture<VrexperiencereservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VrexperiencereservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VrexperiencereservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
