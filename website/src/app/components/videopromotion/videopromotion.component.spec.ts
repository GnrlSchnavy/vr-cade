import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideopromotionComponent } from './videopromotion.component';

describe('VideopromotionComponent', () => {
  let component: VideopromotionComponent;
  let fixture: ComponentFixture<VideopromotionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideopromotionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideopromotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
