import {Component, Input, NgModule, OnInit} from '@angular/core';

@Component({
  selector: 'app-sectiontitle',
  templateUrl: './sectiontitle.component.html',
  styleUrls: ['./sectiontitle.component.css']
})

export class SectiontitleComponent implements OnInit {

  @Input()
  mainTitle: string;
  @Input()
  subTitle: string;
  @Input()
  description: string;


  constructor() { }

  ngOnInit() {
  }

}
