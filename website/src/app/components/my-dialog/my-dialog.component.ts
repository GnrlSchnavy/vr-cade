import {Component, Inject, Input, OnInit} from '@angular/core';
import { MAT_DIALOG_DATA} from '@angular/material';
import { DialogModel } from '../../../models/DialogModel';

@Component({
  selector: 'app-my-dialog',
  templateUrl: './my-dialog.component.html',
  styleUrls: ['./my-dialog.component.css']
})
export class MyDialogComponent implements OnInit {
  @Input() id: number;
  public dialogRef: MyDialogComponent;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogModel ) { }

  ngOnInit() {
  }
}
