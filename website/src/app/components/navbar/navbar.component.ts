import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  constructor(private http: HttpClient) {
  }
  ngOnInit(){

  }

  testbackend() {
    let obs = this.http.get('/api/saveCustomers');
    obs.subscribe((response) => console.log(response));
}
}
