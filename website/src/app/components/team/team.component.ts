import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';
import {MyDialogComponent} from '../my-dialog/my-dialog.component';
import jsonData from '../../../assets/i18n/nl.json';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {
  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  openDialog(naam: string) {
    this.dialog.open(MyDialogComponent, {
      width: '50%',
      data: {
        link: jsonData['ABOUTUS.LINK'],
        title: jsonData['TEAM.' + naam.toString() + '.TITLE'],
        subTitle: jsonData['TEAM.' + naam.toString() + '.SUBTITLE'],
        description: jsonData['TEAM.' + naam.toString() + '.DESCRIPTION'],
        photo: jsonData['TEAM.' + naam.toString() + '.PHOTO'],
        bullets: jsonData['TEAM.' + naam.toString() + '.BULLETS']
      },
    });
  }
}
