import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {BlogComponent} from './components/blog/blog.component';
import {BodyComponent} from './components/body/body.component';
import {RouteguardService} from './services/routeguard/routeguard.service';
import {LoginComponent} from './components/login/login.component';
import {SignupComponent} from './components/signup/signup.component';
import {DefaultreservationComponent} from './components/reservations/defaultreservation/defaultreservation.component';
// @ts-ignore
import {VrexperiencereservationComponent} from './components/reservations/vrexperiencereservation/vrexperiencereservation.component';
import {VrlasergamenreservationComponent} from './components/reservations/vrlasergamenreservation/vrlasergamenreservation.component';
import {VrbedrijfsuitjereservationComponent} from './components/reservations/vrbedrijfsuitjereservation/vrbedrijfsuitjereservation.component';
import {VrracingreservationComponent} from './components/reservations/vrracingreservation/vrracingreservation.component';
import {VrescaperoomreservationComponent} from './components/reservations/vrescaperoomreservation/vrescaperoomreservation.component';
import {VrkinderfeestreservationComponent} from './components/reservations/vrkinderfeestreservation/vrkinderfeestreservation.component';
import jsonData from '../assets/i18n/nl.json';
import {StandardpageComponent} from './components/pages/standardpage/standardpage.component';

const routes: Routes = [
  {path: 'blog', component: BlogComponent, canActivate: [RouteguardService]},
  {path: 'login', component: LoginComponent},
  {path: 'reservations', component: DefaultreservationComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'standardpage', component: StandardpageComponent},
  {path: jsonData['VREXPERIENCE.RESERVATIONLINK'], component: VrexperiencereservationComponent},
  {path: jsonData['VRLASERGAMEN.RESERVATIONLINK'], component: VrlasergamenreservationComponent},
  {path: jsonData['VRBEDRIJFSUITJE.RESERVATIONLINK'], component: VrbedrijfsuitjereservationComponent},
  {path: jsonData['VRRACEN.RESERVATIONLINK'], component: VrracingreservationComponent},
  {path: jsonData['VRESCAPEROOM.RESERVATIONLINK'], component: VrescaperoomreservationComponent},
  {path: jsonData['VRKINDERFEEST.RESERVATIONLINK'], component: VrkinderfeestreservationComponent},
  {path: '', component: BodyComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled', anchorScrolling: 'enabled'})
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})

export class AppRoutingModule {

}
export const routingComponents = [BlogComponent, BodyComponent, DefaultreservationComponent];
