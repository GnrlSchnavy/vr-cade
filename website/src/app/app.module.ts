import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ServicesComponent } from './components/services/services.component';
import { CalltoactionComponent } from './components/calltoaction/calltoaction.component';
import { FeaturesectionComponent } from './components/featuresection/featuresection.component';
import { VideopromotionComponent } from './components/videopromotion/videopromotion.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';
import { PricingComponent } from './components/pricing/pricing.component';
import { CounterComponent } from './components/counter/counter.component';
import { TestimonialsComponent } from './components/testimonials/testimonials.component';
import { TeamComponent } from './components/team/team.component';
import { ClientsComponent } from './components/clients/clients.component';
import { ContactComponent } from './components/contact/contact.component';
import { MapComponent } from './components/map/map.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { AppRoutingModule , routingComponents} from './app-routing.module';
import { SectiontitleComponent } from './components/sectiontitle/sectiontitle.component';
import { SocialComponent } from './components/social/social.component';
import { DefaultreservationComponent } from './components/reservations/defaultreservation/defaultreservation.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { BodyComponent } from './components/body/body.component';
import { ReactiveFormsModule } from '@angular/forms';
import {SubscribeComponent} from './components/subscribe/subscribe.component';
import {HttpClientModule} from '@angular/common/http';
import {TranslateService} from './services/translate/translate.service';
import { TranslatePipe } from './pipes/translate/translate.pipe';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatNativeDateModule} from '@angular/material';
import {MatDialogModule} from '@angular/material';
import { MyDialogComponent } from './components/my-dialog/my-dialog.component';
import { VrexperiencereservationComponent } from './components/reservations/vrexperiencereservation/vrexperiencereservation.component';
import { VrracingreservationComponent } from './components/reservations/vrracingreservation/vrracingreservation.component';
import { VrlasergamenreservationComponent } from './components/reservations/vrlasergamenreservation/vrlasergamenreservation.component';
import { VrbedrijfsuitjereservationComponent } from './components/reservations/vrbedrijfsuitjereservation/vrbedrijfsuitjereservation.component';
import { VrescaperoomreservationComponent } from './components/reservations/vrescaperoomreservation/vrescaperoomreservation.component';
import { VrkinderfeestreservationComponent } from './components/reservations/vrkinderfeestreservation/vrkinderfeestreservation.component';
import { StandardpageComponent } from './components/pages/standardpage/standardpage.component';


@NgModule({
  declarations: [
    AppComponent,
    CarouselComponent,
    NavbarComponent,
    ServicesComponent,
    CalltoactionComponent,
    FeaturesectionComponent,
    VideopromotionComponent,
    PortfolioComponent,
    PricingComponent,
    CounterComponent,
    TestimonialsComponent,
    TeamComponent,
    ClientsComponent,
    ContactComponent,
    MapComponent,
    FooterComponent,
    HeaderComponent,
    SectiontitleComponent,
    SocialComponent,
    LoginComponent,
    SignupComponent,
    routingComponents,
    BodyComponent,
    SubscribeComponent,
    TranslatePipe,
    MyDialogComponent,
    DefaultreservationComponent,
    VrexperiencereservationComponent,
    VrracingreservationComponent,
    VrlasergamenreservationComponent,
    VrbedrijfsuitjereservationComponent,
    VrescaperoomreservationComponent,
    VrkinderfeestreservationComponent,
    StandardpageComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    HttpClientModule,
    MatDialogModule,
    MatButtonModule
  ],
  entryComponents: [
    MyDialogComponent
  ],
  providers: [
    TranslateService,
    {
      provide: APP_INITIALIZER,
      useFactory: setupTranslateFactory,
      deps: [ TranslateService ],
      multi: true
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }

export function setupTranslateFactory(service: TranslateService): Function {
  return () => service.use('nl');
}
